window.addEventListener("DOMContentLoaded", (event) => {
  // Toggle the side navigation
  const sidebarToggle = document.body.querySelector("#sidebarToggle");
  if (sidebarToggle) {
    // Uncomment Below to persist sidebar toggle between refreshes
    // if (localStorage.getItem('sb|sidebar-toggle') === 'true') {
    //     document.body.classList.toggle('sb-sidenav-toggled');
    // }
    sidebarToggle.addEventListener("click", (event) => {
      event.preventDefault();
      document.body.classList.toggle("sb-sidenav-toggled");
      localStorage.setItem(
        "sb|sidebar-toggle",
        document.body.classList.contains("sb-sidenav-toggled")
      );
    });
  }
});

// Filter Cars
        filterSelection("all");

        function filterSelection(c) {
          var x, i;
          x = document.getElementsByClassName("column");
          if (c == "all") c = "";
          for (i = 0; i < x.length; i++) {
            w3RemoveClass(x[i], "show");
            if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
          }
        }

        function w3AddClass(element, name) {
          var i, arr1, arr2;
          arr1 = element.className.split(" ");
          arr2 = name.split(" ");
          for (i = 0; i < arr2.length; i++) {
            if (arr1.indexOf(arr2[i]) == -1) {
              element.className += " " + arr2[i];
            }
          }
        }

        function w3RemoveClass(element, name) {
          var i, arr1, arr2;
          arr1 = element.className.split(" ");
          arr2 = name.split(" ");
          for (i = 0; i < arr2.length; i++) {
            while (arr1.indexOf(arr2[i]) > -1) {
              arr1.splice(arr1.indexOf(arr2[i]), 1);
            }
          }
          element.className = arr1.join(" ");
        }

        // Add active class to the current button (highlight it)
        var btnContainer = document.getElementById("myBtnContainer");
        var btns = btnContainer.getElementsByClassName("btn");
        for (var i = 0; i < btns.length; i++) {
          btns[i].addEventListener("click", function () {
            var current = document.getElementsByClassName("active");
            current[0].className = current[0].className.replace(" active", "");
            this.className += " active";
          });
        }

        function insertChildLink(link) {
          const lower = `${link}`.toLowerCase();
          const ref = lower.replace(/\s/g, '')
              // console.log(ref)
          return `<a class="child-link" href="${ref}.html">${link}</a> <br><br>`
      }
      
      
      function sideNav(linkParent, linkOne) {
          const parentLink = document.querySelector(".parent-link")
          parentLink.innerHTML = linkParent;
      
          const childLink = document.querySelector(".child")
          childLink.innerHTML = insertChildLink(linkOne);
      
          const sideNav = document.getElementById("mySidenav")
          const main = document.getElementById("main")
          console.log(linkParent)
          if (sideNav.style.width == 0 || sideNav.style.width == "0px") {
              sideNav.style.width = "160px"
              sideNav.style.marginLeft = "70px";
              main.style.marginLeft = "230px";
          }
      }
      
      function humberger(currentLink, listLink) {
      
          const parentLink = document.querySelector(".parent-link")
          parentLink.innerHTML = currentLink;
      
          const childLink = document.querySelector(".child")
          childLink.innerHTML = insertChildLink(listLink);
      
          const sideNav = document.getElementById("mySidenav")
          const main = document.getElementById("main")
          if (sideNav.style.width == 0 || sideNav.style.width == "0px") {
              sideNav.style.width = "160px"
              sideNav.style.marginLeft = "70px";
              main.style.marginLeft = "230px";
          } else {
              sideNav.style.width = 0;
              sideNav.style.marginLeft = "0";
              main.style.marginLeft = "70px";
          }
          console.log(sideNav.style.width)
      };